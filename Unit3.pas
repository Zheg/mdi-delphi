unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids;

type
  TForm3 = class(TForm)
    Matrix: TStringGrid;
    Compute1: TButton;
    Label1: TLabel;
    OutCap: TLabel;
    Exit1: TButton;
    procedure Exit1Click(Sender: TObject);
    procedure Compute1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MatrixClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation
uses
  Unit1;

{$R *.dfm}

procedure TForm3.Compute1Click(Sender: TObject);
var
  i, j, index : integer;
  sum, min, max, temp : real;
begin
  if (rg = 0) then
  begin
		i := Matrix.FixedCols;
		index := -1;
		sum := -1.;
		while ((i < Matrix.ColCount) and (sum < 0)) do
    begin
			sum := 0.0;
			for j := Matrix.FixedRows to Matrix.RowCount do
      begin
				if (Matrix.Cells[i, j] <> '') then
        begin
					sum := sum + StrToFloat (Matrix.Cells[i, j]);
				end;
			end;
			i := i + 1;
		end;
		if (sum < 0) then
    begin
			OutCap.Caption := 'Do not exist.';
		end else
    begin
			OutCap.Caption := 'Number of the column = ' + IntToStr (i - 1);
		end
  end else if (rg = 1) then
    begin
		min := StrToFloat(Matrix.Cells[Matrix.FixedCols, Matrix.FixedRows]);
		max := StrToFloat(Matrix.Cells[Matrix.FixedCols, Matrix.FixedRows]);
		for i := Matrix.FixedRows to Matrix.RowCount do
    begin
			if (Matrix.Cells[Matrix.FixedCols, i] <> '') then
      begin
				temp := StrToFloat(Matrix.Cells[Matrix.FixedCols, i]);
				if (temp > max) then
        begin
					max := temp;
				end else if (temp < min) then
        begin
					min := temp;
				end;
			end;
		end;
		for i := Matrix.FixedRows to Matrix.RowCount do
    begin
			if (Matrix.Cells[Matrix.ColCount - 1, i] <> '') then
      begin
				temp := StrToFloat(Matrix.Cells[Matrix.ColCount - 1, i]);
				if (temp > max) then
        begin
					max := temp;
				end else if (temp < min) then
        begin
					min := temp;
				end;
			end;

		end;
		OutCap.Caption := 'Product =  ' + FloatToStr (min * max);
  end else begin
		OutCap.Caption := 'Varinat did not choose.';
  end;
end;

procedure TForm3.Exit1Click(Sender: TObject);
begin
  Close();
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TForm3.MatrixClick(Sender: TObject);
var
 i : integer;
begin
  for i := Matrix.FixedRows to Matrix.RowCount - 1 do
		Matrix.Cells[Matrix.FixedCols - 1, i] := IntToStr(i - Matrix.FixedRows + 1);
	for i := Matrix.FixedCols to Matrix.ColCount - 1 do
		Matrix.Cells[i, Matrix.FixedRows - 1] := intToStr(i - Matrix.FixedCols + 1);

end;

end.
