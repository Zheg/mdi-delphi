object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Dialog'
  ClientHeight = 182
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RG1: TRadioGroup
    Left = 16
    Top = 16
    Width = 305
    Height = 105
    Caption = 'Module'
    Items.Strings = (
      'First column with positive sum.'
      'Product of min and max elements in first and last columns.')
    TabOrder = 0
  end
  object OK1: TButton
    Left = 132
    Top = 144
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = OK1Click
  end
  object Cancel1: TButton
    Left = 246
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Cancel1Click
  end
  object Apply1: TButton
    Left = 16
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Apply'
    TabOrder = 3
    OnClick = Apply1Click
  end
end
