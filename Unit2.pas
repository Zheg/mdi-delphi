unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    RG1: TRadioGroup;
    OK1: TButton;
    Cancel1: TButton;
    Apply1: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Cancel1Click(Sender: TObject);
    procedure Apply1Click(Sender: TObject);
    procedure OK1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses Unit1;

procedure TForm2.Apply1Click(Sender: TObject);
begin
  rg := RG1.ItemIndex;
end;

procedure TForm2.Cancel1Click(Sender: TObject);
begin
  Close();
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Form1.Dialog1.Enabled := True;
end;

procedure TForm2.OK1Click(Sender: TObject);
begin
  rg := RG1.ItemIndex;
  Close();
end;

end.
