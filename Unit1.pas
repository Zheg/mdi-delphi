unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Unit2, Unit3, About;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exi1: TMenuItem;
    Windows1: TMenuItem;
    Control1: TMenuItem;
    Help1: TMenuItem;
    Dialog1: TMenuItem;
    Compute1: TMenuItem;
    Cascade1: TMenuItem;
    ile1: TMenuItem;
    Closecurrent1: TMenuItem;
    Closeall1: TMenuItem;
    About1: TMenuItem;
    procedure About1Click(Sender: TObject);
    procedure Dialog1Click(Sender: TObject);
    procedure Exi1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Compute1Click(Sender: TObject);
    procedure Cascade1Click(Sender: TObject);
    procedure ile1Click(Sender: TObject);
    procedure Closecurrent1Click(Sender: TObject);
    procedure Closeall1Click(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  rg: integer;


implementation

{$R *.dfm}

procedure TForm1.About1Click(Sender: TObject);
begin
  AboutBox.show();
end;

procedure TForm1.Cascade1Click(Sender: TObject);
begin
  Cascade();
end;

procedure TForm1.Closeall1Click(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to MDIChildCount - 1 do
		MDIChildren[i].Close();
end;

procedure TForm1.Closecurrent1Click(Sender: TObject);
begin
  if(ActiveMDIChild <> nil) then
    ActiveMDIChild.Close();
end;

procedure TForm1.Compute1Click(Sender: TObject);
var Child: TForm3;
begin
  Child := TForm3.Create(Application);
end;

procedure TForm1.Dialog1Click(Sender: TObject);
var Child: TForm2;
begin
  Child := TForm2.Create(Application);
  Dialog1.Enabled := False;
end;

procedure TForm1.Exi1Click(Sender: TObject);
begin
  Close();
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  rg := 1;
end;

procedure TForm1.ile1Click(Sender: TObject);
begin
  Tile();
end;

end.
